---
title: "The impact of smartphones and e-mails on our planet"
date: 2020-06-12T09:12:27+01:00
author: "Matita"
draft: false
categories: [Sustainability]
---

I have recently tried to do some research on daily actions and behaviours that have a strong impact on the environment. Searching online, you can find all sorts of recipes to make your own solid shampoo or, if you are a woman, explanations on how to reduce monthly waste by using washable menstrual pads, or lists of things you do not need to buy anymore and that you can produce at home.

All this is remarkable and shows how more and more people around the world try their best every day to live a more green and conscious life.

In this post I want to talk about something that I can't find in those lists, but that has a deep impact on the environment: our electronic devices and the way we use them. 

Most people do not realize how much IT has been contributing to environmental problems, and, to be honest, me neither before starting this research on the world of e-waste. So, let's try to discuss what the key environmental impacts arising from IT are and what everyone can do daily to reduce them.

Let's start with the most basic fact. Manufacturing electronic devices consumes electricity, raw materials, chemicals and water, and generates a great amount of waste. Moreover, the lifespan of many products is becoming shorter, due to the fact that consumers increasingly discard "obsolete" electrical and electronic products 2–3 years after purchase.

But how bad is this attitude? While doing my research, I have come across an article of the United Nations University titled *With E-waste Predicted to Double by 2050, Business as Usual Is Not an Option*[^1].

According to the article, e-waste amounts will more than double from the current 50 million tonnes per year to 110 million tonnes per year by 2050. If you want to have an idea of how much 50 million tonnes is, look at the following picture taken from the Global E-waste Monitor 2017[^2].

![h](/blog/e-waste.png)

That's really a shoking image and given that only 41 countries in the world collect international statistics on e-waste, we can imagine that the situation is even worse than we can think. Moreover, with only about 20 per cent of that waste recycled, most of the e-waste generated is not even properly treated. In fact, the remaining 40 million tonnes of e-waste are discarded in landfill, burned or illegally traded and treated in a sub-standard way every year[^2].

This leads to major problems like

- **Improper and unsafe treatment and disposal**
E-waste is often exported to developing countries where workers are not trained or equiped to proper recycle it. Proper processing is essential to ensure that hazardous components are not released into the environment, contaminating air, water, and soil, and putting people’s health at risk. 

- **Huge Amounts of Raw Materials Are Wasted**
The improper handling of e-waste is resulting in a significant
loss of scarce and valuable raw materials[^3]. According to the Global E-waste Monitor 2017, raw materials present in e-waste is estimated
to be worth 55 Billion Euros in 2016.

- **Illegal International Movement of Hazardous Waste**
Illegal trafficking of waste has become a significant source of revenue of criminal organizations. The United Nations Environment Programme (UNEP) estimates that organised crime earned approximately US$20-30 billion from environmental crimes[^4].

The situation is not idyllic. There are responsabilities that have to be taken on different levels. 

Many countries do not have a national legislation on e-waste. In fact, only 67 countries have legislation in place to deal with the e-waste they generate. 

Manufacturers too have a role to play by making it easier to reduce waste by standardizing common components such as power supply cords and chargers.

In this scenario where politics and companies do not take responsabilities for the environment cause, you could feel unarmed. What could you possibly do if at the top level the situation is so bad?

Well, here are some ideas

- Think twice before replacing your phone or any other electronic device. Could you make it work for say another six months? Do it.
- When searching for a new device, consider to look at the refurbished section that some stores offer. Often, you can find very good deals which will save you some money as well.
- Do some research before buying something. For instance, while searching for material for this article, I came across the brand [**FAIRPHONE**](https://www.fairphone.com/en/) which creates fairer and more sustainable smartphones. The principle is to make modular smartphones built with recycled and responsibly mined materials. Modularity ensures you can repair it and do not throw it away just because, for instance, the battery is no longer properly charging. Search for such brands and, if you can afford it, give them a try.

As a software engineer, I have to warn you that not only hardware has an impact on the environment, software does as well. This is something we do not talk so much about, but that can play a crucial role.

Servers, computers and data centre cooling systems require electrical energy to work. The increasing amount of information that we send and receive every day makes this consumption of energy result in greater CO2 emissions, as most electricity is generated by burning fossil fuel like coal, oil and gas. 

At this point you could think what all this has to do with you, right? You do not control servers and you think that there is nothing you can do about it. Well, that's wrong. Have you ever thought of how many unnecessary e-mails we receive and send everyday, from subscriptions to spam, from thank you emails to pointless email chains? Well, every e-mail is information that needs to be moved from one location to another and it requires electricity to do it.

Mike Berners-Lee in his book [*How Bad are Bananas?: The Carbon Footprint of Everything*](https://www.amazon.com/How-Bad-Are-Bananas-Everything-ebook/dp/B004VO4IZY/ref=sr_1_2?dchild=1&keywords=How+Bad+are+Bananas%3F%3A+The+Carbon+Footprint+of+Everything&qid=1591253339&sr=8-2) writes

>*A typical year of incoming mail adds up to 135 kg CO2e: over 1 per cent of the 10-tonne lifestyle and equivalent to driving 200 miles in an average car.*

More specifically, the author explains how a normal email has a footprint equivalent to 0.3 g of CO2 emissions which can rise to 50g for e-mails with the addition of a large attachment. This measure takes into account everything from the power in data centres to the computers that send, filter and read the messages.

Again, what can you do to be more respectful of the environment?

- **Cutting down on the number of unnecessary emails** you send is one way to reduce the CO2 emitted by your emails. 
- **Flush the spam folder and declutter your inbox**. Deleting these messages frees up space on the servers that store email data.
- **Unsubscribe from channels you do not care about anymore** and **think twice before subscribing**. Is it really worth it or could you make the extra little effort of going to the specific page to look for updates?
- If you are a blogger, **remove the contacts that unsubscribe, and update changed email addresses immediately**. This will avoid notifications about undelivered messages from being sent.
- **Reduce the size of emails** either by sending e-mails in a text format instead of html and by lowering the resolution and compressing images.

Is there anything else? 

With the powerful and accurate cameras integrated in our smartphones at our disposal, we take pictures of everything. We do not even care about memory because we synchronize our pictures directly through the Cloud. So, what can we do to reduce the impact this has on the environment without giving up on taking pictures?

Perhaps you would like to declutter your pictures folder from time to time. 

Deleting all the Whatsapp images of cute cats can save some space as well. 

Once per year, you could decide to include in your spring cleaning, a device cleaning and stock your pictures you do not have to have with you all the time on a hard disk, instead of keeping them on some server somewhere in the world.

I hope this article has given you some ideas on how we can really make a different with small changes in our life.

[^1]: [With E-waste Predicted to Double by 2050, Business as Usual Is Not an Option](https://unu.edu/news/news/with-e-waste-predicted-to-double-by-2050-business-as-usual-is-not-an-option.html)
[^2]: [Global-E-waste Monitor 2017](https://www.itu.int/en/ITU-D/Climate-Change/Documents/GEM%202017/Global-E-waste%20Monitor%202017%20.pdf)
[^3]: [A New Circular Vision for Electronics](http://www3.weforum.org/docs/WEF_A_New_Circular_Vision_for_Electronics.pdf)
[^4]: Walters, Reece (2013) [Eco Mafia and Environmental Crime](https://link.springer.com/chapter/10.1057%2F9781137008695_19)
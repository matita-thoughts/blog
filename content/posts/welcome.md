---
title: "Welcome!"
date: 2020-03-23T22:12:27+01:00
author: "Matita"
draft: false
categories: []
---

Hi! I’m Matita, a former mathematician and currently a sotware developer and technical team lead. It has been a very long time I had the idea of having my own blog, but never the courage to go public. I have many interests – from programming languages to natural languages, from gardening to crocheting and DIY – and I love to share my experiences with creating!

Writing, in particular, is one of my favorite activities and this blog is my big challenge. Until now, I have been writing to keep in touch with my friends and share with them experiences about trips, new jobs and hiring processes (I have changed quite sometime) or just new hobbies I have found myself interested in. I will share more about my motivation and the decision to open up in an upcoming post.

I will cover different topics about things I have learned or that I find interesting and hope this blog can be of some help. The range can be wide, but I will always write about subjects I do know. I will always encourage you to comment in order to improve the quality of the posts or if you would like to see more posts about a particular subject.

I am very excited to start this journey and I hope you will enjoy it!

See you soon!
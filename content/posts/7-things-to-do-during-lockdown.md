---
title: "7 things to do during lockdown"
date: 2020-04-21T22:12:27+01:00
author: "Matita"
draft: false
categories: [Self-care]
---

In order to slow down the spread of the corona virus, governments all over the world have adopted a complete lockdown of their countries. This has forced many of us to sit tight at home for quite a long period.

Many people perceive this period as an intolerable deprivation of their freedom, but what if we look at it as an opportunity?

>*"The greatest obstacle to living is expectancy, which hangs upon tomorrow and loses today" (Lucius Annaeus Seneca the Younger)*

In this spirit, here are seven ideas of how to make the most of this situation and get you through the weeks of lockdown.

1. **Keep Learning**

If like me, you consider yourself a forever student, this is definitely the perfect opportunity to learn new skills and widen your knowledge.&nbsp;There are plenty of online courses on basically anything, so the choice is hard. My suggestion is to make a list of the three topics that interest you the most and you start. Here is my list, maybe you will find something interesting.

- Learn how to create a static website with [Hugo](https://gohugo.io/). *This first goal has been achieved, the new blog with no ads and totally free and 100% developed by myself is coming soon* 😊.

- Learn more about Python and how to use it to build a machine learning model for the [COVID-19 dataset](https://www.kaggle.com/) available on the Kaggle website. I had the chance to have my company give employees access to the platform [Pluralsight](https://www.pluralsight.com/). I am currently enrolled on two classes: *Core Python: Getting Started* (to refresh some concepts) and *Building Machine Learning Models* in Python with scikit-learn. I am learning a lot and I am glad I joined them. For those who do not have access to private platforms, this is no good excuse not to start. There are plenty of free alternatives, [Coursera](https://www.coursera.org/) or [FutureLearn](https://www.futurelearn.com/) are just examples, just pick one 😉

- Learning Spanish! I bought a book of [ELi](https://www.elionline.com/) collection of beautifully illustrated classics, specially adapted for language learners (another very good alternative is [Black Cat Cideb](https://www.blackcat-cideb.com/en/about-us/)), a couple of years ago. The book has an audio recording of the text, read by a native speaker. It's a shame to keep it on the shelf for such a long time. But now, I have plenty of time to dedicate to it. If you do not have a text-book at home, this is not a good time to buy one, but what can definitely help are websites like [Duolingo](https://en.duolingo.com/) or [Bosuu](https://www.busuu.com/) to brushing up on vocabulary, and testing out pronunciation, and it is FUN!

2. **Get creative – get a new hobby**

Well, as I said, for me the first option has been to start a blog. If you are interested in cooking or programming, or feeling passionate about movies and series and you like to share your knowledge with others, you should give it a try. Or, why not think even bigger? If you always had in mind to write a story, your story, a novel or a book, this is the right occasion to start. Not tomorrow, but today. Just start putting down sparse ideas and you will see how words will flow soon more easily. I have read that in some posts people claim that Shakespeare wrote King Lear while in isolation during the plague. Well, that's not actually true and I invite you to read this nice article [*Shakespeare in lockdown: did he write King Lear in plague quarantine?*](https://www.theguardian.com/stage/2020/mar/22/shakespeare-in-lockdown-did-he-write-king-lear-in-plague-quarantine) in The Guardian. I would like to share with you just two reflections about this new "legend":

- Beware of fake news. They are everywhere and they can hurt you! I will write more about this topic in a subsequent post, stay tuned 😉
- Does it really matter? Think that everything you are doing, you are doing it for you, because it gives you joy! Don't put pressure on yourself!

Another hobby I found particularly effective to get my mind off stress and pressure is to draw. This is a very cheap and powerful way to pause. Circles, flowers, abstract forms or scribbles, there are infinite choices. What about a drawing per day?

Finally, if you feel you want to do something more manual. Why not starting gardening? It does not matter if you live in a small apartment. Possibilities are countless. I have tried with an avocado seed some weeks ago with the method of the toothpicks and guess what? It is growing! For most of the fruits and vegetables you have at home, for the first 10 days you do not even need soil in order for the plant to germinate. If you are curious, just keep the seed in a moist paper towel for a week and see what happens.

Taking care of plants is really rewarding. Every new day is not the same, but it involves patience and attention. If you have a plant at home, start looking at it in the morning, water it gently, clean dry leaves, re-pot it if it needs more space, look online and get more information on how to take care of it. That's a very mindful exercise!

3. **Read the books you never had time for**

Does it happen to you to keep a book on the shelf simply because you want to read it calmly? A book that you do not feel like to read while waiting for a train on the way back from work, just because it deserves all your attention and a particular atmosphere? Well, do not wait any longer and grab it. This is a good chance to relax with the company of a good book.

If you have already done it and you are uncertain about your next best choice, you can have a look at [Internet Archive](https://archive.org/index.php) and explore their catalog of public domain books.

If I can give you my personal advice, try the *Moral Letters to Lucilius* of  Lucius Annaeus Seneca the Younger. It could really change the perspective on the way we look at our life and it is still extremely relevant.

4. **Get some time for yourself**

No morning rush means more time for you. Do no waste it! It will not come back!  Establish a morning routine where you take 15 minutes and you do not look at your smartphone or tablet, no social medias, no news. We are constantly planning something, doing something and getting worried about tomorrow, thinking about when life will be back to normal, that we forget to take the time for ourselves. If you are not into yoga and meditation, you can decide to invest these 15 minutes for stretching, or praying (yes, praying is a valid option) or just drink your coffee silently. No interaction, no distraction. How many times does it happen that you drink a tea and you do not take the time to smell it, feel the warmth from the cup to your hands and finally the taste in your mouth? Next time, try to make this simple action in a more conscious way. You will see how much relaxed you will be afterwards! Simple aware actions can really change your day!

5. **Enjoy new recipes**

Many people are developing bad habits during lockdown, eating basically anything at hand while watching news or looking at social media posts. At the end of the day, someone will feel guilty, but this feeling is not enough to encourage us to change our habits.

I think the best way to change this vicious circle, is *again* to put some consciousness in your life. I just mentioned in the previous list point that a simple way to take some time for yourself is simply drinking a hot beverage and involve all the senses while doing it. Let's go a bit further. Why instead of rapidly putting a tea bag in your cup, you do not prepare a simple infusion yourself with spices you have at home? I recently tried this one: ginger, lemon and rosemary. While cutting the ginger in tiny pieces or pressing the lemon, feel the freshness of the ingredients and the pleasant aroma of simplicity. Remember, it is not just the drink you are preparing, but the fact that you are doing it for yourself that matters. You are taking care of yourself.

This approach on food as the pleasure of nourishing our bodies could encourage you to experiment with new recipes. Look at the nutritional values of the ingredients, try to privilege local and seasonal products, reduce the amount of salt and let spices give flavor and exalt the taste of your food. Reducing the amount of sugar when preparing  cookies or cakes can also be a very good and healthy way to modify recipes found online. Lately, I have tried it myself. I am a real addict of [Ikea oat biscuits](https://www.ikea.com/us/en/p/kafferep-oat-biscuits-chocolate-utz-certified-60374893/), and I wanted to try making them myself. If you prepare your own snacks you have an impact on the environment (think of the thick paper boxes you throw away every time you empty a package), as well as on the quality of the ingredients. These Ikea biscuits are really easy to make, but while searching for a recipe, I have found some of them putting more sugar and butter than flour 😱.

Well, you do not need a lot of flour for those biscuits, but still I do not want to eat pure sugar. So what I did is to drastically reduce the suggested amount of sugar and butter and replaced it with my grandma's walnut flour. Walnut flour contains some fat and gives a nice taste to the biscuits! Definitely happy with my choice and next time I would reduce the quantity of sugar even more! I will write a post on the positive impact of sugar reduction on your body and your mind soon.

6. **Take an online tour of a museum**

I am very glad that art galleries and museums around the world have decided to give access to their collections and offer free virtual tours during this period. I love art and this is really an incredible opportunity available to anyone. 

Just sit comfortably on your couch and enjoy the magnificent Renaissance collections of [Gallerie degli Uffizi](https://artsandculture.google.com/partner/uffizi-gallery?hl=en) in Florence, or be amazed by the colors of Van Gogh and Cezanne’s paintings or by the masterpieces of the impressionists at [Paris’ Musée d’Orsay](https://artsandculture.google.com/partner/musee-dorsay-paris?hl=en), or by the gold and the elegant lines of  Klimt's artworks at [Vienna's Secession](https://artsandculture.google.com/entity/vienna-secession/m04zh55?hl=en). Never been to New York and fan of  modern art? Just take a tour of [New York’s MoMA](https://artsandculture.google.com/partner/moma-the-museum-of-modern-art?hl=en). Would you like to have more insights on the artifacts from around the world by choosing a particular thematic path, like *Trade and conflicts* or *Power and identity*, or by following the path of a particular continent? [The British Museum](https://britishmuseum.withgoogle.com/) is what you are looking for.

Of course, the list above is not exhaustive and reflects my personal taste, but on the [Google’s arts and culture collection](https://artsandculture.google.com/partner?hl=en) you can choose among over 500 attractions around the world, including national galleries and individual artist museums.

7. **Deep clean and declutter you home**

This last idea is really meant to make you aware of how we end up collecting stuff over the years we do not really need and we are incapable of letting go. How many times did it happen to open a drawer and find a key-chain, a T-shirt or a booklet that you had even forgotten you owned? If you forgot those items, probably you do not really need them, right? 

I will talk about minimalism and the positive impact it can have in your life in a future post. For the moment, I would like to quote the words of Giorgio Armani in an open letter he wrote to [*WWD Women’s Wear Daily*](https://wwd.com/fashion-news/designer-luxury/giorgio-armani-writes-open-letter-wwd-1203553687/).

>The reflection on how absurd the current state of things is, with the overproduction of garments and a criminal nonalignment between the weather and the commercial season, is courageous and necessary.This crisis is an opportunity to slow down and realign everything; to define a more meaningful landscape.
>
>This crisis is also an opportunity to restore value to authenticity: Enough with fashion as pure communication, enough with cruise shows around the world to present mild ideas and entertain with grandiose shows that today seem a bit inappropriate, and even a tad vulgar — enormous but ultimately meaningless wastes of money.
(Giorgio Armani)

So, if you were thinking "Where should I start?", why not start with your wardrobe? I am pretty sure you have clothes you never wear.&nbsp;Why not take the time to identify them and discard those you never wore for years? Afterwards, you can move to the kitchen and see how many bottle openers you have collected, how many mugs, etc and start discarding those items you do not use anymore.

This process really helps you to rethink how much of your stuff you really need, make some order and let you enjoy the space you live in. Living with only the things we really need instead of pilling up items also means less to clean and less to organize. Therefore, more time!


Think of the joy you give to people who will receive those things you do not need or you do not value anymore. There are many people suffering during this pandemic crisis and I think it is our moral duty to help, when we can.


I hope you enjoyed this post and that it gave you some ideas to face this situation with a different spirit.
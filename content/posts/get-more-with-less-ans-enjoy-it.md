---
title: "Get more with less... and enjoy it!"
date: 2020-05-04T12:12:27+01:00
author: "Matita"
draft: false
categories: [Minimalism]
---

Sometimes we get the feeling that even though we have absolutely everything we need, we don't have enough. Decades of commercials have had a great impact on our society and led us to believe that with the right watch, the right perfume, the right car we are on the way to success. Those are simply *must-haves*.

I have to admit, I have always found this must-have concept quite ridiculous. Must means, well, you know, what must means. Like when the alarm rings and you want to sleep more, but you *must* get out of your bed.

What I really find striking is that we are not even conscious of the fact that this wanting more attitude prevents us from fully enjoying our life. We are in perennial dissatisfaction. Let's look at some examples.

We like to watch cooking shows on TV. At some point, we want to try because it looks fun and good and tasty. Yes, let's cook! We start cutting some vegetables and we notice that we do not have that amazing Japanese knife that the chef is using in the video, and look at the blender, yours is just a 3 speed one (he has 10!) and yours does not have the Total Crushing Technology. And, you know what? His kitchen is as big as your whole apartment. That makes you feel not so enthusiastic anymore. In this scenario, there are those who give up and those who start buying everything they think is a must-have without which cooking is impossible. And yes, the Japanese knife is one of those things.

Now, let's move back to work. You know that the image you give of yourself is of primary importance, especially if you do business. It is Monday morning, you are at work and you are taking a break (say cigarette or coffee pause) with your colleagues. In the Monday morning chitchat, the following, evergreen question arises: "Have you done something interesting during the weekend?". Nothing wrong with that, right? It is just a simple question. Well, have you ever heard somebody replying "I've been cleaning the apartment, laid on the couch and watched TV?". Personally, rarely. And the person who dares to do it gets labeled as *boring*. 

Unfortunately, this is not an isolated case. Pages and pages of magazines and blog posts about the importance of new experiences and trips, some of them suggesting a list of things to accomplish and places to visit before you hit your 20s and your 30s. This pushes us to constantly fill our life with something new. The same consideration applies to traveling. Approaching summertime, the question about holidays is a constant. The competition for the most trendy location chosen for spending holidays started. 

Constantly conforming, organizing, packing, moving. It seems to me that we are afraid of stopping. If we do it, we are obliged to look what is really in our lives and this can be scary. But it is inevitable. At some point, life's going to ask us for the bill. You think that I am exaggerating, that this kind of stress is innocuous! FALSE.


Studies have shown that chronic stress impacts cognition and increases vulnerability to&nbsp;mental illness.[^1] [^2] Scientific research examined how stress affects our body and health. The stress hormone *corticosteroid* can suppress the effectiveness of the immune system[^3]. This means that when we are stressed, the immune system’s ability to fight off antigens is reduced. That is why we are more susceptible to infections. Stress can also lead to some unhealthy habits that have a negative impact on our health. Cumulative stress can result in individual adaptations that decrease self-control, increase impulsivity, and increase risk for addiction.[^4] [^5] For example, many people cope with stress by eating too much or by smoking. These unhealthy habits damage the body and create bigger problems in the long-term.


The good news is that there is a simple way to snap out of it and the solution is at hand. Let's use a quotation to explain how (I love quotations):

>*Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma - which is living with the results of other people's thinking. Don't let the noise of other's opinions drown out your own inner voice. And most important, have the courage to follow your heart and intuition. They somehow already know what you truly want to become. Everything else is secondary.* (Steve Jobs)

Though, I am not a big fan of Steve Jobs, this one is really powerful. And, it is true. You can't be happy with somebody else setting your priorities and lifestyle. You would continue searching for more, doing more, but in the end you would not really know why you have made those choices. Simply because they are not yours.

Instead, start living an intentional life. A life where you do not let others decide what is fashionable and what should make you happy. A life where you slowly cut things out until you are left only with what you love, with what is necessary, with what really makes you happy.

This sounds very theoretical and, if you start it all at once, you could feel overwhelmed and not so effective. Let's try with some examples.  

You are chopping some vegetables and you realize that that gorgeous Japanese knife is cool. It shines and it seems it can cut evrything. At this very moment, ask yourself this simple question: "Do I really need it?" Think of it in a rational way. Open the cupboard and look at the cutlery that you already own. Wouldn't be sufficient to sharpen the ones you already have? I hope you start seeing the point. Do not perceive it as a deprivation. You just need to answer the question in an honest way. Maybe, the answer is yes and then you will buy it. End of story. The only major, crucial difference is that you have done it **consciously**. 

You can repeat the same algorithm over and over again. You will see how easy it becomes after some time. You will also experience the reward the first time you decide that something is superflous and you decide not to buy it. You will also start valuing and appreciating what is around you. You will look at things with a different eye. 

Does this way of life only applies to material things? Absolutely not. We are full of obligations with which we fill our life. In an attempt to conform to external models, we lose the codes of happiness and feeling of well-being. Think for a moment at all the conversations we have on a daily basis. Those colleagues asking about weekend and holidays, how does it sound? Well, what if once per week you avoid those kind of conversations? You go for a walk or sit quietly somewhere? Those 10 minutes you spend with yourself have a positive impact on the rest of the day. You don't believe me? Try it.

I hope you will start decluttering soon and enjoy your life with what really matters!


[^1]: Marie-France Marin, Catherine Lord, Julie Andrews, Robert-Paul Juster, Shireen Sindi, Geneviève Arsenault-Lapierre, Alexandra J. Fiocco, Sonia J.Lupien (2011) [Chronic stress, cognitive functioning and mental health](https://www.sciencedirect.com/science/article/abs/pii/S1074742711000517)

[^2]: Thomas Frodl, Veronica O'Keane (2013). [How does the brain deal with cumulative stress? A review with focus on developmental stress, HPA axis function and hippocampal structure in humans](https://www.sciencedirect.com/science/article/abs/pii/S0969996112000836)

[^3]: Bruce S. McEwen (2008). [Central effects of stress hormones in health and disease: understanding the protective and damaging effects of stress and stress mediators](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2474765/?)

[^4]: Emily B. Ansell, Peihua Gu, Keri Tuit, and Rajita Sinha (2012). [Effects of cumulative stress and impulsivity on smoking status](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3582663/)

[^5]: Tanya M. Spruill (2010). [Chronic Psychosocial Stress and Hypertension](https://link.springer.com/article/10.1007/s11906-009-0084-8)